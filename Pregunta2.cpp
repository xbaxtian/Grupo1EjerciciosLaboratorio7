#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <conio.h>
using namespace std;

struct nodo{
	int valor;
	struct nodo *sgte;
	struct nodo *ant;
};

typedef struct nodo *Tlista;

void insertarFinal(Tlista &lista);
void insertarFinalCircular(Tlista &lista, int dato);
void insertarFinalSimple(Tlista &lista, int dato);
void crearlistas(Tlista &lista, Tlista &lista_circular, Tlista &lista_simple);
int promedio(Tlista &lista);
void mostrarcircular(Tlista &f);
void mostrar (Tlista &lista);
void programa(Tlista &f, Tlista &lista_circular, Tlista &lista_simple);
string menu(Tlista &lista);
int convertir(string num, bool &bandera);

int main(){

	Tlista lista = NULL;
	Tlista lista_circular=NULL;
	Tlista lista_simple=NULL;
	int op,numero;
	programa(lista, lista_circular, lista_simple);
}

void programa(Tlista &f, Tlista &lista_circular, Tlista &lista_simple)
{
    string op;
    do
    {
        op = menu(f);

        if(op.size() > 1 || op.size() == 0)
            cout << "\tHas escrito demasiados caracteres!"<<endl;
        else
        {
            if(op.compare("1") == 0)
            {
                cout<<"\n\tHas elegido la opcion 1"<<endl;
                insertarFinal(f);

            }
            else if(op.compare("2") == 0)
            {
                cout<<"\n\tHas elegido la opcion 2"<<endl;
                crearlistas(f,lista_circular,lista_simple);
				cout<<"\n\tLista Circular:"<<endl;
				mostrarcircular(lista_circular);
				cout<<"\n\tLista Simple: "<<endl;
				mostrar(lista_simple);
            }
            else if(op.compare("3") == 0)
            {
				cout<<"\n\tHas elegido la opcion 3"<<endl;
				mostrar(f);
            }
            else if(op.compare("4") == 0)
            {
				cout<<"\n\tHas elegido la opcion 4"<<endl;
				cout<<"\n\tLista Circular:"<<endl;
				mostrarcircular(lista_circular);
				cout<<"\n\tLista Simple: "<<endl;
				mostrar(lista_simple);
            }
            else if(op.compare("5") == 0)
            {
				cout<<"\n\tHas elegido la opcion 5..."<<endl;
				cout<<"\tprocediendo a salir..."<<endl;
            }

        }

        cin.ignore(256,'\n');
    }while(op != "5");
}

string menu(Tlista &f)
{
    system("cls");
    string op;
    	cout<<endl;
    	cout<<"\t浜様様様様様様様様様様様様様様様様様様様様様様様�"<<endl;
        cout<<"\t�\t\t\t\t\t\t�"<<endl;
        cout<<"\t�\t\t\tMENU\t\t\t�"<<endl;
        cout<<"\t�\t\t\t\t\t\t�"<<endl;
        cout<<"\t�\t1. Agregar elemento a la lista principal�"<<endl;
        cout<<"\t�\t2. Crear listas\t\t\t\t�"<<endl;
        cout<<"\t�\t3. Mostrar lista principal\t\t�"<<endl;
        cout<<"\t�\t4. Mostrar lista circular y simple\t�"<<endl;
        cout<<"\t�\t\t\t\t\t\t�"<<endl;
        cout<<"\t�\t5. Salir\t\t\t\t�"<<endl;
        cout<<"\t�\t\t\t\t\t\t�"<<endl;
        cout<<"\t藩様様様様様様様様様様様様様様様様様様様様様様様�"<<endl;

        mostrar(f);
        cout<<endl;

        cout<<"\n\tSeleccionar opcion: ";
        cin>>op;


        cin.ignore(256,'\n');
    return op;
}

int promedio(Tlista &lista)
{
    Tlista p=lista;
    int n=0,s=0,prom=0;
    while(p!=NULL)
    {
        n++;
        s=s+p->valor;
        p=p->sgte;
    }
    prom = s/n;
    return prom;
}

 void insertarFinal(Tlista &lista){

	Tlista p = lista;
	int numero;
	bool f;
	Tlista nuevo = new (struct nodo);
	nuevo->sgte = NULL;
	nuevo->ant=NULL;

	string numero_aux;
	cout<<"\tIngrese un numero: "; cin>>numero_aux;

	numero = convertir(numero_aux,f);

	if(f == 0)
	{
		cout << "\tIngreso un numero Invalido";
		getch();
		return;
	}

	nuevo->valor = numero;

	if(lista == NULL){
		lista = nuevo;

	}else{
		if(lista->sgte == NULL){
			lista->sgte = nuevo;
			nuevo->ant = lista;

		}else{
			while(p->sgte != NULL){
				p = p->sgte;
			}
			p->sgte = nuevo;
			nuevo->ant = p;

		}
	}

}

void insertarFinalSimple(Tlista &lista, int dato)
{
    Tlista p = lista;
	Tlista nuevo = new (struct nodo);
	nuevo->sgte = NULL;
	nuevo->ant=NULL;
	nuevo->valor = dato;

	if(lista == NULL){
		lista = nuevo;

	}else{
		if(lista->sgte == NULL){
			lista->sgte = nuevo;

		}else{
			while(p->sgte != NULL){
				p = p->sgte;
			}
			p->sgte = nuevo;
		}
	}

}

void insertarFinalCircular(Tlista &lista, int dato)
{
    Tlista p = lista;
	Tlista nuevo = new (struct nodo);
	nuevo->sgte = NULL;
	nuevo->ant=NULL;
	nuevo->valor = dato;

	if(lista == NULL){
		lista = nuevo;
		nuevo->sgte=nuevo;

	}else{
		if(lista->sgte == lista){
			lista->sgte = nuevo;
			nuevo->sgte = lista;

		}else{
			while(p->sgte != lista){
				p = p->sgte;
			}
			p->sgte = nuevo;
			nuevo->sgte = lista;
		}
	}
}


void crearlistas(Tlista &lista, Tlista &lista_circular, Tlista &lista_simple)
{
   // cout<<"si"<<endl;
    Tlista p=lista;
    int m=promedio(lista);
    while(p!=NULL)
    {
        if(p->valor<=m)
        {
            insertarFinalCircular(lista_circular, p->valor);
        }
        else
        {
            insertarFinalSimple(lista_simple, p->valor);
        }
        p=p->sgte;
    }
}

void mostrarcircular(Tlista &f)
{

    if(f==NULL)
    {
        cout<<"\n\tLista vacia"<<endl;
        return;
    }
    Tlista p=f;
    cout<<"\tLista : "<<endl<<endl;
    cout<<"\t";
    while(p->sgte!=f)
    {
        cout<<p->valor<<" - ";
        p=p->sgte;
    }

    cout<<p->valor<<endl;
}

void mostrar(Tlista &f)
{
    if(f==NULL)
    {
        cout<<"\n\tLista vacia"<<endl;
        return;
    }
    Tlista p=f;
    cout<<"\tLista : "<<endl<<endl;
    cout<<"\t";
    while(p->sgte!=NULL)
    {
        cout<<p->valor<<" - ";
        p=p->sgte;
    }

    cout<<p->valor<<endl;
}

int convertir(string num, bool &bandera)//La bandera me indica si fue correcto, con 1 -> fue un exito, con 0 -> fracaso
{
    int aux = 0;
    bandera = true;

    for(int i = 0 ; i < num.size() ; i++)
    {

        if((int) num.at(i) >= 48 && (int) num.at(i) <= 57 )
        {
            aux += ( (int)num.at(i) - 48) * pow(10,num.size() - (i+1) );
        }
        else
        {
            bandera = false;
            aux = -999;
            break;
        }

    }

    return aux;
}
