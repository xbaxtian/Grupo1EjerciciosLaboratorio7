#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <conio.h>

using namespace std;

struct nodo{
	char valor;
	struct nodo *sgte;
};

typedef struct nodo *Tlista;

void insertarFinal(Tlista &lista, char letra);
void mostrar (Tlista &lista);
void programa(Tlista &lista);
string menu(Tlista &f);
int convertir(string num, bool &bandera);
void cambiar(Tlista &f, int &n, int &m);


int main(){
	
	Tlista f = NULL;
	int numero;
	programa(f);
}

void programa(Tlista &f)
{
	int cont=0;
    string op;
    do
    {
        op = menu(f);

        if(op.size() > 1 || op.size() == 0)
            cout << "\tHas escrito demasiados caracteres!"<<endl;
        else
        {
            if(op.compare("1") == 0)
            {
            	if(cont ==0){
            		cout<<"\n\tHas elegido la opcion 1"<<endl;
                	insertarFinal(f,'R');
                	insertarFinal(f,'A');
                	insertarFinal(f,'M');
               		insertarFinal(f,'O');
               		insertarFinal(f,'N');
               		cont++;
				}else{
					cout<<"\n\tYa se creo la lista RAMON"<<endl;
				}
                
            }
            else if(op.compare("2") == 0)
            {
                cout<<"\n\tHas elegido la opcion 2"<<endl;
                int n,m;
                bool e1,e2;
				string n_aux,m_aux;
				
				if(f!= NULL){
						cout<<"\n\tIngrese un posicion inicial: "; cin>>n_aux;
					n = convertir(n_aux,e1);	
					if(e1 == 0)
					{
						cout << "\n\tIngreso un numero Invalido";
						getch();
					}else{
						cout<<"\tIngrese un posicion final: "; cin>>m_aux;
						m = convertir(m_aux,e2);
						if(e2 == 0)
						{
							cout << "\tIngreso un numero Invalido";
							getch();
						}else{
							if(n>0 && n<=5 && m>0 && m<=5){
								cambiar(f,n,m);	
							}else{
								cout<<"Los valores exceden el tama�o de la lista (5)"<<endl;
								getch();
							}
						}
					}
				}else{
					cout<<"\n\tLa lista esta vacia."<<endl;
				}
					
            }
			else if(op.compare("3") == 0)
            {
				cout<<"\n\tHas elegido la opcion 3"<<endl;
                mostrar(f);
            }
            else if(op.compare("4") == 0)
            {
				cout<<"\n\tHas elegido la opcion 4..."<<endl;
				cout<<"\tprocediendo a salir..."<<endl;
            }

        }

        cin.ignore(256,'\n');
    }while(op != "4");
}

string menu(Tlista &f)
{
    system("cls");
    string op;
    	cout<<endl;
        cout<<"\t�����������������������������������������������ͻ"<<endl;        
        cout<<"\t�\t\t\t\t\t\t�"<<endl;
        cout<<"\t�\t\t\tMENU\t\t\t�"<<endl;
        cout<<"\t�\t\t\t\t\t\t�"<<endl;
        cout<<"\t�\t   1. Crear Lista RAMON\t\t\t�"<<endl;
        cout<<"\t�\t   2. Cambiar manualmente\t\t�"<<endl;
        cout<<"\t�\t   3. Mostrar\t\t\t\t�"<<endl;
        cout<<"\t�\t   4. Salir\t\t\t\t�"<<endl;
        cout<<"\t�\t\t\t\t\t\t�"<<endl;
        cout<<"\t�����������������������������������������������ͼ\n"<<endl;

        mostrar(f);

        cout<<"\n\tSeleccionar opcion: ";
        cin>>op;


        cin.ignore(256,'\n');
    return op;
}

 void insertarFinal(Tlista &lista, char letra){
	
	Tlista p = lista;
	Tlista nuevo = new (struct nodo);
	nuevo->sgte = NULL;
	
	nuevo->valor = letra;
	
	if(lista == NULL){
		lista = nuevo;
	}else{
		if(lista->sgte == NULL){
			lista->sgte = nuevo;
		}else{
			while(p->sgte != NULL){
				p = p->sgte;
			}
			p->sgte = nuevo;
		}
	}
}

void mostrar(Tlista &f)
{
    if(f==NULL)
    {
        cout<<"\n\tLista vacia"<<endl;
        return;
    }
    Tlista p=f;
    cout<<"\tLista : "<<endl<<endl;
    cout<<"\t";
    while(p->sgte!=NULL)
    {
        cout<<p->valor<<" - ";
        p=p->sgte;
    }
    
    cout<<p->valor<<endl;
}

int convertir(string num, bool &bandera)//La bandera me indica si fue correcto, con 1 -> fue un exito, con 0 -> fracaso
{
    int aux = 0;
    bandera = true;

    for(int i = 0 ; i < num.size() ; i++)
    {

        if((int) num.at(i) >= 48 && (int) num.at(i) <= 57 )
        {
            aux += ( (int)num.at(i) - 48) * pow(10,num.size() - (i+1) );
        }
        else
        {
            bandera = false;
            aux = -999;
            break;
        }

    }

    return aux;
}

void cambiar(Tlista &f, int &n, int &m){
	Tlista p= f;
	Tlista u= f;
	int i=1, j=1;
	
	while(i!=n){
		p=p->sgte;
		i++;
	}
	
	cout<<u->valor;
	while(j!=m){
		u=u->sgte;
		j++;
	}
	
	Tlista aux= new (struct nodo);
	aux->valor = p->valor;
	p->valor = u->valor;
	u->valor = aux->valor;
}
