#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include<conio.h>
#include<math.h>
using namespace std;
void Quick_Sort(int arreglo[], int inicio, int fin);
int partir(int arreglo[], int inicio, int fin);

void menu();
void programa();

int convertir(string num, bool &bandera);
int main()
{

    programa();


    return 0;
}

void Quick_Sort(int arreglo[], int inicio, int fin)
{
    int pivote;

    if (inicio < fin)
    {
        pivote = partir(arreglo, inicio, fin);

        Quick_Sort(arreglo, inicio, pivote - 1);

        Quick_Sort(arreglo, pivote + 1, fin);
    }

}

int partir(int arreglo[], int inicio, int fin) {
    int izq;
    int der;
    int pivote;
    int temp;

    pivote = arreglo[inicio];
    izq = inicio;
    der = fin;

    while (izq < der) {
        while (arreglo[der] > pivote) {
            der--;
        }

        while ((izq < der) && (arreglo[izq] <= pivote)) {
            izq++;
        }

        if (izq < der) {
            temp = arreglo[izq];
            arreglo[izq] = arreglo[der];
            arreglo[der] = temp;
        }
    }

    temp = arreglo[der];
    arreglo[der] = arreglo[inicio];
    arreglo[inicio] = temp;

    return der;
}



void programa()
{
    string op;
	do
	{

	    int n;
	    bool bandera;

		menu();

		cout << "\n\tIngrese el tama�o del arreglo: ";
		cin >> op;
		n = convertir(op,bandera);
		if(bandera)
        {
          int arreglo[n];
          int i = 0 ;
          for(i; i < n ; i++)
          {
              cout << "\n\tIngrese el elemento nro "<<i+1<<" : ";
              cin>>op;
              int temp;
              temp = convertir(op,bandera);
              if(bandera)
                arreglo[i] = temp;
                else
                {
                    cout << "\n\tIngreso un numero invalido, intentelo de nuevo";
                    getch();
                    break;
                }

              cin.ignore(256, '\n');
          }

          if(bandera)
          {
              cout <<"\n\tEl arreglo inscrito es : ";

              for(int i = 0 ; i< n ; i++)
                cout << arreglo[i] << "|" ;

              cout << "\n\n\tPresione una tecla para ordenar por Quick Sort"<<endl;
              getch();

              Quick_Sort(arreglo,0,n-1);

              cout <<"\n\tEl arreglo ordenado es : ";

              for(int i = 0 ; i< n ; i++)
                cout << arreglo[i] << "|" ;

              cout << "\n\n\tDesea salir? (SI,NO)"<<endl;
              cin>>op;
              cin.ignore(256, '\n');
              getch();
          }

        }


		cin.ignore(256, '\n');
	} while (op != "NO" && op != "No" && op!="no");
}
void menu()
{
	system("cls");
	cout << endl;
	cout<<endl;
        cout<<"\t�����������������������������������������������ͻ"<<endl;
        cout<<"\t�\t\t\t\t\t\t�"<<endl;
        cout<<"\t�\t\t\tMENU\t\t\t�"<<endl;
        cout<<"\t�\t\t\t\t\t\t�"<<endl;
        cout<<"\t�  Pregunta 3 Ordenamiento con Quick Sort\t�"<<endl;
        cout<<"\t�\t\t\t\t\t\t�"<<endl;
        cout<<"\t�����������������������������������������������ͼ\n"<<endl;

	cout<<"\n\t\t";
	cout << endl;



}

int convertir(string num, bool &bandera)
{
	int aux = 0;
	bandera = true;

	for (int i = 0; i < num.size(); i++)
	{

		if ((int)num.at(i) >= 48 && (int)num.at(i) <= 57)
		{
			aux += ((int)num.at(i) - 48) * pow(10, num.size() - (i + 1));
		}
		else
		{
			bandera = false;
			aux = -999;
			break;
		}

	}

	return aux;
}
