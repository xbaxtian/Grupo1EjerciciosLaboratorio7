#include <iostream>
#include <string>
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
using namespace std;


struct nodo
{
	struct nodo *ant;
	char dato;
	struct nodo *sgte;
};


typedef struct nodo *Lista;

Lista crearElemento(char valor);

void ingresarFinal(Lista &cab, char valor);
void ingresarPosicion(Lista &cab, char valor, int pos);
void ingresarInicio(Lista &cab, char valor);
void ingresarEntre(Lista &cab,char var1, char var2, char entrada);

void mostrar(Lista &cab,int opcion);
bool ingresarCaracter(char &entrada);

string menu(Lista &cab);
void programa(Lista &cab);


int main()
{
	Lista cab = NULL;

	programa(cab);

	getch();

	return 0;

}

void programa(Lista &cab)
{
	string op;
	do
	{
		op = menu(cab);



		if (op.size() > 1 || op.size() == 0)
			cout << "\tHas escrito demasiados caracteres!" << endl;
		else
		{
			if (op.compare("1") == 0)
			{
				char s;
				bool bandera;

				bandera = ingresarCaracter(s);

				if(bandera)
				{
					ingresarFinal(cab,s);
				}

			}
			else if (op.compare("2") == 0)
			{
				mostrar(cab,1);
				getch();
			}
			else if (op.compare("3") == 0)
			{
				mostrar(cab,2);
				getch();
			}
			else if (op.compare("4") == 0)
			{
				char var1,var2,var3;
				bool b1,b2,b3;

                cout<< "\n\tIngrese el primer Caracter"<<endl;
				b1 = ingresarCaracter(var1);
				cout<< "\n\tIngrese el segundo Caracter"<<endl;
				b2 = ingresarCaracter(var2);
				cout<< "\n\tIngrese el caracter del Medio"<<endl;
				b3 = ingresarCaracter(var3);

				if(b1 && b2 && b3)
				{
					ingresarEntre(cab,var1,var2,var3);
					//getch();
				}

			}
			else if (op.compare("5") == 0)
			{
				char s;
				bool bandera;

				bandera = ingresarCaracter(s);

				if(bandera)
				{
					ingresarInicio(cab,s);
				}
			}
			else if (op.compare("6") == 0)
			{
				cout << "\n\tHas elegido la opcion 6..." << endl;
				cout << "\tprocediendo a salir..." << endl;
			}

		}

		cin.ignore(256, '\n');
	} while (op != "6");
}
string menu(Lista &cab)
{
	system("cls");
	string op;
	cout << endl;
	cout << "\t浜様様様様様様様様様様様様様様様様様様様様様様様" << endl;
	cout << "\t�\t\tMENU\t\t\t\t�" << endl;
	cout << "\t�\t   1. Crear Lista\t\t\t�" << endl;
	cout << "\t�\t   2. Mostrar de Derecha a Izquierda\t�" << endl;
	cout << "\t�\t   3. Mostrar de Izquierda a Derecha\t�" << endl;
	cout << "\t�\t   4. Ingresar entre dos Caracteres \t�" << endl;
	cout << "\t�\t   5. Ingresar al Inicio \t\t�" << endl;
	cout << "\t�\t   6. Salir\t\t\t\t�" << endl;
	cout << "\t様様様様様様様様様様様様様様様様様様様様様様様様�\n" << endl;

	cout<<"\n\t\t";
	mostrar(cab,1);
	cout << endl;
	cout << "\n\tSeleccionar opcion: ";
	cin >> op;

	cin.ignore(256, '\n');
	return op;

}
bool ingresarCaracter(char &entrada)
{
	string aux;
	cout << "\tIngrese un caracter: ";
	cin>>aux;

	bool bandera;

	//cin.ignore(256, '\n');

	if(aux.size() != 1)
	{
		cout << "\tIngreso demasiados caracteres";
		getch();
		return false;
	}
	else
	{
		entrada = aux.at(0);
		return true;
	}

}

void ingresarEntre(Lista &cab,char var1, char var2, char entrada)
{
	if(cab == NULL)
		cout << "\tLa lista esta vacia";
	else
	{
		Lista temporal = cab;
		int i = 1;

		while(temporal->dato != var1 && temporal->dato != var2 && temporal != NULL)
		{
			temporal = temporal->sgte;
			i++;
		}
		if(temporal == NULL)
			cout << "\tNo se encontraron los datos";
		else if(temporal->sgte == NULL)
			cout << "\tNo se encontraron los datos";
		else
		{
			if(temporal->dato == var1)
				if(temporal->sgte->dato != var2)
					cout << "\tNo son dos valores adyacentes";
				else
					ingresarPosicion(cab,entrada,i+1);
			else if(temporal->dato == var2)
				if(temporal->sgte->dato != var1)
					cout << "\tNo son dos valores adyacentes";
				else
					ingresarPosicion(cab,entrada,i+1);
		}
	}
}

void mostrar(Lista &cab,int opcion)
{
	Lista aux = cab;
	if (cab == NULL)
		cout << "La lista esta vacia";
	else
	{
		if(opcion == 1)
		{
		    cout << "\t\t";
			while (aux->sgte != NULL)
			{
				cout << aux->dato << "|";
				aux = aux->sgte;
			}
			cout << aux->dato;
		}
		else
		{
		    cout << "\t\t";
			while(aux->sgte != NULL)
				aux = aux->sgte;
			while (aux->ant != NULL)
			{
				cout << aux->dato << "|";
				aux = aux->ant;
			}
			cout << aux->dato;
		}

	}
}


void ingresarInicio(Lista &cab, char valor)
{
	if (cab == NULL)
		cab = crearElemento(valor);
	else
	{
		Lista aux = crearElemento(valor);
		aux->sgte = cab;
		cab->ant = aux;
		cab = aux;
	}
}


Lista crearElemento(char valor)
{
	Lista aux = new(struct nodo);
	aux->dato = valor;
	aux->sgte = NULL;
	aux->ant = NULL;
	return aux;
}
void ingresarFinal(Lista &cab, char valor)
{
	if (cab == NULL)
		cab = crearElemento(valor);
	else
	{
		Lista aux = cab;

		while (aux->sgte != NULL)
			aux = aux->sgte;

		Lista temp = crearElemento(valor);
		aux->sgte = temp;
		temp->ant = aux;
	}
}

void ingresarPosicion(Lista &cab, char valor, int pos)
{
	int c = 1;
	if (pos <= 0)
		cout << "Posicion Invalida";
	else if (pos == 1)
		ingresarInicio(cab, pos);
	else
	{
		Lista aux = cab;
		while (aux->sgte != NULL && pos != c)
		{
			aux = aux->sgte;
			c++;
		}
		if (pos == c)
		{
			Lista temp = crearElemento(valor);
			aux->ant->sgte = temp;
			temp->ant = aux->ant;
			temp->sgte = aux;

		}
		else if (pos == (c + 1))
		{
			Lista temp = crearElemento(valor);
			temp->ant = aux;
			aux->sgte = temp;
		}

	}

}

