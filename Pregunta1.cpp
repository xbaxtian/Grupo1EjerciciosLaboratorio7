#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <conio.h>
#include<string.h>

using namespace std;

struct nodo{
	char valor;
	struct nodo *sgte;
};

typedef struct nodo *Tlista;

void insertarFinal(Tlista &lista, char valor);
void Eliminar_inicio(Tlista &lista);
void Eliminar_posicion(Tlista &lista, int valor);
void mostrar (Tlista &lista);
void programa(Tlista &lista);
int menu(Tlista &lista);
int convertir(string num, bool &bandera);
int main(){

	Tlista lista = NULL;
	Tlista lista_2=NULL;
	int op,numero;
	programa(lista);
}

void programa(Tlista &f)
{
int x,op,pos;
char valor;
bool b1;
string cad;
    do
    {
        op=menu(f);
        string s;
        switch(op)
        {
        case 1:
            cout<<"\n\tIngrese un valor: ";

            cin>>s;
            if(s.size()==1)
            {
                valor = s.at(0);
                int aux = int(valor);

                if( (aux >=65 && aux <= 90) || (aux >=97 && aux <=122) )
                {
                    insertarFinal(f,valor);
                    mostrar(f);

                }
                else{
                    cout << "\n\tUsted no ingreso una letra"<<endl;
                    getch();
                }


            }
            break;
        case 2:
            cout<<"\n\tIngrese la posicion a eliminar: ";
            cin>>cad;
            x= convertir(cad, b1);
            if(b1==1){
                Eliminar_posicion(f,x);
                break;
            }else{
                cout<<"\n\tFormato incorrecto."<<endl;
            getch();
                break;
            }
            break;
        case 3:
            cout<<"\n\nMostrando lista..."<<endl;
            mostrar(f);
            break;
        case 4:
            break;
        default:
            cout<<"algo anda mal"<<endl;
            getch();
        }

    }while(op!=4);
}

int menu(Tlista &f)
{
    system("cls");
    string op;
    	cout<<endl;
    	cout<<"\t浜様様様様様様様様様様様様様様様様様様様様様様様�"<<endl;
        cout<<"\t�\t\t\t\t\t\t�"<<endl;
        cout<<"\t�\t\t\tMENU\t\t\t�"<<endl;
        cout<<"\t�\t\t\t\t\t\t�"<<endl;
        cout<<"\t�\t1. Agregar elemento a la cinta\t\t�"<<endl;
        cout<<"\t�\t2. Quitar Elemento de la cinta\t\t�"<<endl;
        cout<<"\t�\t3. Mostrar\t\t\t\t�"<<endl;
        cout<<"\t�\t\t\t\t\t\t�"<<endl;
        cout<<"\t�\t4. Salir\t\t\t\t�"<<endl;
        cout<<"\t�\t\t\t\t\t\t�"<<endl;
        cout<<"\t藩様様様様様様様様様様様様様様様様様様様様様様様�"<<endl;


        mostrar(f);
        cout<<endl;

        cout<<"\n\tSeleccionar opcion: ";
        cin>>op;

        bool t;
        int aux = convertir(op,t);

        if(t)
            return aux;
        else
            return -9999;

        cin.ignore(256,'\n');
}

 void insertarFinal(Tlista &lista, char valor){

	Tlista p = lista;
	int numero;
	bool f;
	Tlista nuevo = new (struct nodo);
	nuevo->sgte = NULL;

	nuevo->valor = valor;

	if(lista == NULL){
		lista = nuevo;
	}else{
		if(lista->sgte == NULL){
			lista->sgte = nuevo;
		}else{
			while(p->sgte != NULL){
				p = p->sgte;
			}
			p->sgte = nuevo;
		}
	}
}

void Eliminar_inicio(Tlista &lista)
{
    Tlista p=lista;
    if(lista==NULL)
    {
        cout<<"Lista vacia, imposible eliminar mas"<<endl;
        return;
    }
    else
    {
        lista=p->sgte;
        delete(p->sgte);
    }
}

void Eliminar_posicion(Tlista &lista, int valor)
{
    Tlista p=lista, q=NULL, aux=lista;
    int n=1;
    int c = 0;

    while(p != NULL)
    {
        c ++;
        p = p->sgte;

    }

    if(valor > c)
        return;

    p = lista;


    if(lista==NULL)
    {
        cout<<"\tNo hay elementos en la cinta\n"<<endl;
        system("pause");
        return;
    }
    while(p!=NULL)
    {


        if(n==valor)
        {
            Eliminar_inicio(lista);
        }
        else
        {
            insertarFinal(q,p->valor);
            Eliminar_inicio(lista);
           // mostrar(q);
        }

        p=p->sgte;
        n++;
    }
    while(q!=NULL)
    {
        insertarFinal(lista,q->valor);
        q=q->sgte;
    }
    cout<<"\tLa cinta queda asi: "<<endl;
    mostrar(lista);
    system("pause");
}

void mostrar(Tlista &f)
{
    if(f==NULL)
    {
        cout<<"\n\tLista vacia"<<endl;
        return;
    }
    Tlista p=f;
    cout<<"\tLista : "<<endl<<endl;
    cout<<"\t";
    while(p->sgte!=NULL)
    {
        cout<<p->valor<<" - ";
        p=p->sgte;
    }

    cout<<p->valor<<endl;
}

int convertir(string num, bool &bandera)//La bandera me indica si fue correcto, con 1 -> fue un exito, con 0 -> fracaso
{
    int aux = 0;
    bandera = true;

    for(int i = 0 ; i < num.size() ; i++)
    {

        if((int) num.at(i) >= 48 && (int) num.at(i) <= 57 )
        {
            aux += ( (int)num.at(i) - 48) * pow(10,num.size() - (i+1) );
        }
        else
        {
            bandera = false;
            aux = -999;
            break;
        }

    }

    return aux;
}
